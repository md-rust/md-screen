fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
    return vec4<f32>(in.colour, 1.0);
}
