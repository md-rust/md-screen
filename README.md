# md-screen

This crate provides a simple API for creating a window that can display data based on a WGPU fragment shader.
The crate draws a rectangle that fills the screen, allowing the fragment shader to determine the colour of each pixel.
The shader is passed various parameters such as screen size etc.

The crate also doubles up as an event loop which can pass events to the application via the `ScreenApp` trait.

# Usage

First you create a screen via a ScreenBuilder:

```rust
use md_screen::ScreenBuilder

...


let screen = ScreenBuilder::new()
    .with_inner_size(800, 600),
    .with_title("My screen"),
    .exit_on_escape()
    .build();
```

Secondly, you attach the shader with all the uniforms:

```rust
// TO BE DETERMINED!
```

Finally, you call the `run` method to execute the event loop:

```rust
screen.run();
```

# Purpose

This crate is designed to be the basis of `md-term`, a crate that allows fast ASCII rendering, which in turn is the basis
of a roguelike game I am building.

