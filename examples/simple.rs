//
// Simple example that sets colour based on coordinates
//

use anyhow::{Context, Result};
use md_screen::ScreenBuilder;

fn main() -> Result<()> {
    let screen = ScreenBuilder::new()
        .with_inner_size(1024, 768)
        .with_title("md-screen simple example")
        .exit_on_escape()
        .with_shader(include_str!("simple.wgsl"))
        .build();

    screen.run().with_context(|| "Unable to create screen")
}
