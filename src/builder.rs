#![allow(dead_code)]

//
// ScreenBuilder structure
//

use crate::Screen;

pub struct ScreenBuilder {
    pub(crate) size: (usize, usize),
    pub(crate) title: String,
    pub(crate) exit_on_escape: bool,
    pub(crate) shader: String,
}

impl ScreenBuilder {
    pub fn new() -> Self {
        ScreenBuilder {
            size: (800, 600),
            title: String::from("md-screen"),
            exit_on_escape: false,
            shader: String::new(),
        }
    }

    pub fn with_inner_size(self, width: usize, height: usize) -> Self {
        ScreenBuilder {
            size: (width, height),
            ..self
        }
    }

    pub fn with_title(self, title: &str) -> Self {
        ScreenBuilder {
            title: String::from(title),
            ..self
        }
    }

    pub fn with_shader(self, shader: &str) -> Self {
        ScreenBuilder {
            shader: String::from(shader),
            ..self
        }
    }

    pub fn exit_on_escape(self) -> Self {
        ScreenBuilder {
            exit_on_escape: true,
            ..self
        }
    }

    pub fn build(self) -> Screen {
        Screen::new(self)
    }
}
