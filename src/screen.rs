#![allow(dead_code)]
#![allow(unused_variables)]

use futures::executor::block_on;
use thiserror::Error;
use wgpu::{Device, Instance, Queue, RenderPipeline, Surface, SwapChain, SwapChainDescriptor};
use winit::{
    dpi::PhysicalSize,
    event::{Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

use crate::ScreenBuilder;

//
// Screen structure
//

pub struct Screen {
    builder: ScreenBuilder,
    state: Option<State>,
}

impl Screen {
    pub fn new(builder: ScreenBuilder) -> Self {
        Screen {
            builder,
            state: None,
        }
    }

    pub fn run(mut self) -> Result<(), Error> {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_inner_size(PhysicalSize::new(
                self.builder.size.0 as f32,
                self.builder.size.1 as f32,
            ))
            .with_title(self.builder.title.clone())
            .build(&event_loop)
            .map_err(|_| Error::CannotCreateWindow)?;
        let exit_on_escape = self.builder.exit_on_escape;
        block_on(self.create_screen(&window))?;

        event_loop.run(move |ev, _, control_flow| {
            *control_flow = ControlFlow::Wait;

            match ev {
                Event::WindowEvent { event, window_id } if window_id == window.id() => {
                    // Handle all window events
                    if !self.input(&event) {
                        match event {
                            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                            WindowEvent::KeyboardInput { input, .. } => match input {
                                KeyboardInput {
                                    virtual_keycode: Some(code),
                                    state: winit::event::ElementState::Pressed,
                                    ..
                                } => match code {
                                    // Handle all keyboard events involving pressed keys
                                    VirtualKeyCode::Escape if exit_on_escape => {
                                        *control_flow = ControlFlow::Exit
                                    }
                                    _ => {}
                                },
                                _ => {}
                            },
                            WindowEvent::Resized(physical_size) => {
                                self.resize(physical_size);
                            }
                            WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                                self.resize(*new_inner_size);
                            }
                            _ => {}
                        }
                    }
                }
                Event::MainEventsCleared => {
                    self.update();
                    window.request_redraw();
                }
                Event::RedrawRequested(_) => {
                    let render_result = self.render();
                    match render_result {
                        Ok(_) => {}
                        Err(Error::SwapChainError(err)) => match err {
                            wgpu::SwapChainError::Lost => {
                                if let Some(ref state) = self.state {
                                    let size = state.size;
                                    self.resize(size);
                                }
                            }
                            wgpu::SwapChainError::OutOfMemory => *control_flow = ControlFlow::Exit,
                            _ => eprintln!("{:?}", err),
                        },
                        Err(err) => eprintln!("{:?}", err),
                    }
                }
                _ => {}
            }
        });
    }

    //
    // Creation
    //

    async fn create_screen(&mut self, window: &Window) -> Result<(), Error> {
        let instance = Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
            })
            .await
            .ok_or_else(|| Error::CannotCreateAdapter)?;
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None,
            )
            .await
            .map_err(|_| Error::CannotCreateDevice)?;
        let size = window.inner_size();
        let swap_chain_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            format: adapter
                .get_swap_chain_preferred_format(&surface)
                .ok_or_else(|| Error::CannotCreateSwapChain)?,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &swap_chain_desc);

        let shader_src = format!("{}{}", include_str!("shader.wgsl"), self.builder.shader);

        let shader = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
            label: Some("Shader"),
            flags: wgpu::ShaderFlags::all(),
            source: wgpu::ShaderSource::Wgsl(shader_src.into()),
        });
        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[],
                push_constant_ranges: &[],
            });
        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "main",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "main",
                targets: &[wgpu::ColorTargetState {
                    format: swap_chain_desc.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrite::ALL,
                }],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: None,
                polygon_mode: wgpu::PolygonMode::Fill,
                clamp_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
        });

        self.state = Some(State {
            surface,
            device,
            queue,
            swap_chain_desc,
            swap_chain,
            size,
            render_pipeline,
        });

        Ok(())
    }

    //
    // Resizing
    //

    fn resize(&mut self, new_size: PhysicalSize<u32>) {
        if let Some(ref mut state) = self.state {
            state.size = new_size;
            state.swap_chain_desc.width = new_size.width;
            state.swap_chain_desc.height = new_size.height;
            state.swap_chain = state
                .device
                .create_swap_chain(&state.surface, &state.swap_chain_desc);
        }
    }

    //
    // Render
    //

    fn render(&self) -> Result<(), Error> {
        if let Some(ref state) = self.state {
            // Get the texture we will render into.
            let frame = state.swap_chain.get_current_frame()?.output;

            // Create a CommandEncoder to create the commands that we need to send to the GPU.
            let mut encoder =
                state
                    .device
                    .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                        label: Some("md-screen GPU encoder"),
                    });

            // Clear the screen

            // Render pass mutably borrows the encoder so we scope it
            {
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: Some("Main render pass"),
                    color_attachments: &[wgpu::RenderPassColorAttachment {
                        view: &frame.view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Clear(wgpu::Color {
                                r: 0.1,
                                g: 0.2,
                                b: 0.3,
                                a: 1.0,
                            }),
                            store: true,
                        },
                    }],
                    depth_stencil_attachment: None,
                });

                render_pass.set_pipeline(&state.render_pipeline);
                render_pass.draw(0..4, 0..1);
            }

            state.queue.submit(std::iter::once(encoder.finish()));
        }

        Ok(())
    }

    //
    // Input
    // Returns true if the input was taken
    //

    fn input(&mut self, _event: &WindowEvent) -> bool {
        false
    }

    //
    // Update
    //

    fn update(&mut self) {}
}

//
// Screen errors
//

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not create window to host screen.")]
    CannotCreateWindow,

    #[error("Could not create the GPU adapter.")]
    CannotCreateAdapter,

    #[error("Could not create GPU device.")]
    CannotCreateDevice,

    #[error("Unable to construct the swap chain.")]
    CannotCreateSwapChain,

    #[error("Swapchain error.")]
    SwapChainError(wgpu::SwapChainError),
}

impl From<wgpu::SwapChainError> for Error {
    fn from(err: wgpu::SwapChainError) -> Self {
        Error::SwapChainError(err)
    }
}

//
// WGPU usage and data
//

struct State {
    surface: Surface,
    device: Device,
    queue: Queue,
    swap_chain_desc: SwapChainDescriptor,
    swap_chain: SwapChain,
    size: PhysicalSize<u32>,
    render_pipeline: RenderPipeline,
}
